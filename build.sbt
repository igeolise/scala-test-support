organization := "com.igeolise"

name := "scala-test-support"

version := "1.2.0"

scalaVersion := "2.12.7"

crossScalaVersions := Seq("2.11.12", "2.12.7")

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",  // yes, this is 2 args
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-Ywarn-unused-import"
)

igeoliseResolverSettings

libraryDependencies := Seq(
  "org.joda" % "joda-convert" % "1.8",
  "joda-time" % "joda-time" % "2.9.4",
  "org.scalatest" %% "scalatest" % "3.0.5",
  "org.mockito" % "mockito-core" % "1.10.19",
  "org.hamcrest" % "hamcrest-core" % "1.3"
)
