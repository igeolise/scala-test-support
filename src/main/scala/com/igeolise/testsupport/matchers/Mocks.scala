package com.igeolise.testsupport.matchers

import org.mockito.Mockito._
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import org.mockito.{Matchers => M}
import org.scalatest.mockito.MockitoSugar
import org.joda.time.DateTime

/**
 * Created with IntelliJ IDEA.
 * User: arturas
 * Date: 11/7/12
 * Time: 5:27 PM
 * To change this template use File | Settings | File Templates.
 */
trait Mocks extends MockitoSugar {
  def chilledMock[T <: AnyRef : Manifest] = super.mock[T]
  def angryMock[T <: AnyRef](implicit manifest: Manifest[T]) = {
    val mock =
      super.mock(new UnstubbedInvocationAnswer(manifest.runtimeClass))(manifest)
    doReturn(s"AngryMock[$manifest]").when(mock).toString
    mock
  }

  // Angry mock that will shout at you if you invoke unknown stuff on it.
  def mock[T <: AnyRef : Manifest] = angryMock[T]

  // Time precision in milliseconds
  private[this] val TimePrecision = 1000

  case class SpiedLeft[T](what: T, code: T => Unit)

  implicit class AnyMatchers[T](val o: T) {
    def eqM = M.eq(o)
  }

  implicit class DateTimeMatchers(val instant: DateTime) {
    def within(millis: Int) = M.argThat(
      new Within[DateTime](instant, millis, identity)
    )
    def near = within(TimePrecision)
  }

  protected class MatchDateTimeEpsilon(instant: DateTime) extends Equals {
    override def canEqual(that: Any) = that.isInstanceOf[DateTime]

    override def equals(that: Any) =
      new Within[DateTime](instant, TimePrecision, identity).matches(that)

    override def toString = s"$instant +/- $TimePrecision ms"
  }

  protected implicit class DateTimeEpsilonMatcher(instant: DateTime) {
    def unary_~() = new MatchDateTimeEpsilon(instant)
  }

  def spied[T](what: T)(code: T => Unit) = SpiedLeft(what, code)
}

class UnstubbedInvocationException(msg: String) extends RuntimeException(msg)

class UnstubbedInvocationAnswer(klass: Class[_]) extends Answer[Nothing] {
  def answer(invocation: InvocationOnMock) = {
    val paramTypes = invocation.getMethod.getParameterTypes
    val args = invocation.getArguments
    val argStr = args.zip(paramTypes).map { case (arg, pType) =>
      s"$arg:${pType.getCanonicalName}"
    }.mkString(", ")

    throw new UnstubbedInvocationException(
      s"Unknown mock method invocation: ${klass.getCanonicalName}#${
      invocation.getMethod.getName}($argStr)"
    )
  }
}
